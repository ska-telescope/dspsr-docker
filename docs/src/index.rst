SKA-PST-DSP-TOOLS
=================

This project provides Docker images for offline analysis of data recorded by the SKA Pulsar Timing (PST) backend.
Two Docker images are built for use as an interactive shell: ``ska-pst-psrchive`` and ``ska-pst-dspsr``.

ska-pst-psrchive
----------------

The ``ska-pst-psrchive`` base image provides the `PSRCHIVE <https://psrchive.sourceforge.net/>`_ suite of tools
for the analysis of pulsar astronomical data in the form of phase-resolved averages known as folded pulse profiles.
The installed tools include the development library, python bindings, and user-end executables.
In addition to all of the build tools required to compile the code base, a number of third-party packages are
also installed, including PGPLOT, FFTW3, CFITSIO, Armadillo, splinter, and yaml-cpp.
Although not strictly dependencies, vim and gnuplot are also installed.

Pulsar-specific software packages (psrchive, tempo2, and psrcat) are installed in ``$HOME/Pulsar``.

ska-pst-dspsr
-------------

This ``ska-pst-dspsr`` image builds on top of ``ska-pst-psrchive`` and provides the
`DSPSR <https://dspsr.sourceforge.net/>`_ suite of tools for the analysis of pulsar astronomical time series data.
The installed tools include the development library, python bindings, and user-end executables.
In addition to DSPSR, `PSRDADA <https://psrdada.sourceforge.net/>`_ is also installed to help
with managing PST voltage recorder data files.  The dspsr library is configured with CUDA support for four
generations of Nvidia GPUs.

Pulsar-specific software packages (dspsr and psrdada) are installed in ``$HOME/Pulsar``.


.. README =============================================================

.. toctree::
  :maxdepth: 1
  :caption: Readme
  :hidden:

  ../../README.md

.. COMMUNITY SECTION ==================================================

..

.. toctree::
  :maxdepth: 2
  :caption: Usage
  :hidden:

  usage/index

