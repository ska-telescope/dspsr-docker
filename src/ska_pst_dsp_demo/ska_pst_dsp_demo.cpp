/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cuda_runtime.h>
#include <cassert>

#include "dsp/Detection.h"
#include "dsp/DetectionCUDA.h"
#include "dsp/TimeSeries.h"
#include "dsp/TransferCUDA.h"
#include "dsp/MemoryCUDA.h"
#include "Types.h"

auto main() -> int try
{
  #ifdef _DEBUG
  dsp::Operation::verbose = true;
  #endif

  dsp::TimeSeries * input = new dsp::TimeSeries;
  dsp::TimeSeries * output = new dsp::TimeSeries;
  dsp::Detection detection_cpu;

  unsigned nchan{4}, npol{2}, ndim{2}, ndat{16384};
  std::cerr << "Configuring input timeseries with nchan=" << nchan << " npol=" << npol << " ndim=" << ndim << " ndat=" << ndat << std::endl;

  input->set_nchan(nchan);
  input->set_npol(npol);
  input->set_ndim(ndim);
  input->resize(ndat);
  input->set_state(Signal::Analytic);
  input->set_order(dsp::TimeSeries::OrderFPT);

  for (unsigned ichan=0; ichan<nchan; ichan++)
  {
    for (unsigned ipol=0; ipol<npol; ipol++)
    {
      float * ptr = input->get_datptr(ichan, ipol);
      for (unsigned idat=0; idat<ndat; idat++)
      {
        for (unsigned idim=0; idim<ndim; idim++)
        {
          ptr[idat*ndim + idim] = float(idat);
        }
      }
    }
  }

  detection_cpu.set_input(input);
  detection_cpu.set_output(output);
  detection_cpu.set_output_state(Signal::Intensity);
  detection_cpu.prepare();
  detection_cpu.operate();

  int gpu_device = 0;
  std::cerr << "Using GPU device " << gpu_device << std::endl;
  cudaSetDevice(gpu_device);

  cudaStream_t stream;
  std::cerr << "Creating CUDA Stream" << std::endl;
  cudaError_t result = cudaStreamCreate(&stream);

  std::cerr << "Preparing timeseries and transformations" << std::endl;
  dsp::Detection detection_gpu;
  dsp::TimeSeries * input_gpu = new dsp::TimeSeries;
  dsp::TimeSeries * output_gpu = new dsp::TimeSeries;

  CUDA::DeviceMemory * device_memory = new CUDA::DeviceMemory(stream, gpu_device);
  input_gpu->set_memory(device_memory);
  output_gpu->set_memory(device_memory);

  std::cerr << "Copy input timeseries from host to device" << std::endl;
  dsp::TransferCUDA h2d(stream);
  h2d.set_kind(cudaMemcpyHostToDevice);
  h2d.set_input(input);
  h2d.set_output(input_gpu);
  h2d.operate();

  std::cerr << "Performing CUDA Detection" << std::endl;
  detection_gpu.set_input(input_gpu);
  detection_gpu.set_output(output_gpu);
  detection_gpu.set_output_state(Signal::Intensity);
  detection_gpu.set_engine(new CUDA::DetectionEngine(stream));

  detection_gpu.prepare();
  detection_gpu.operate();

  std::cerr << "Performing Device To Host Transfer" << std::endl;
  dsp::TimeSeries * output_cpu = new dsp::TimeSeries;
  dsp::TransferCUDA d2h(stream);
  d2h.set_kind(cudaMemcpyDeviceToHost);
  d2h.set_input(output_gpu);
  d2h.set_output(output_cpu);
  d2h.operate();

  #ifdef _DEBUG
  std::cerr << "output nchans=" << output->get_nchan() << "," << output_cpu->get_nchan() << std::endl;
  std::cerr << "output npols=" << output->get_npol() << "," << output_cpu->get_npol() << std::endl;
  std::cerr << "output ndims=" << output->get_ndim() << "," << output_cpu->get_ndim() << std::endl;
  std::cerr << "output ndats=" << output->get_ndat() << "," << output_cpu->get_ndat() << std::endl;
  #endif

  std::cerr << "Checking Computed Results" << std::endl;
  for (unsigned ichan=0; ichan<nchan; ichan++)
  {
    float * cpu = output->get_datptr(ichan, 0);
    float * gpu = output_cpu->get_datptr(ichan, 0);
    for (unsigned idat=0; idat<ndat; idat++)
    {
      // compute the square law (Re*Re + Im*Im) for both polarisations
      float square_law_power = float(npol * ndim * idat * idat);
      assert(cpu[idat] == square_law_power);
      assert(gpu[idat] == square_law_power);
    }
  }
  return 0;
}
catch (Error exc)
{
  std::cerr << exc << std::endl;
}
