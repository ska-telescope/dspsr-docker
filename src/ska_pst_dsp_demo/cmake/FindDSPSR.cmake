# Find the DSPSR library
#
# The following are set after configuration is done:
#  DSPSR_INCLUDE_DIR
#  DSPSR_CFLAGS
#  DSPSR_LDFLAGS

find_path(DSPSR_BIN_PATH
    NAMES dspsr_cflags
    HINTS
    ${PATH})

execute_process (
    COMMAND bash -c ${DSPSR_BIN_PATH}/dspsr_cflags
    OUTPUT_VARIABLE DSPSR_CFLAGS
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

execute_process (
    COMMAND bash -c ${DSPSR_BIN_PATH}/dspsr_ldflags
    OUTPUT_VARIABLE DSPSR_LDFLAGS
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

set(DSPSR_INCLUDE_DIR ${DSPSR_BIN_PATH}/../include)
set(DSPSR_LDFLAGS ${DSPSR_LDFLAGS})
set(DSPSR_CFLAGS ${DSPSR_CFLAGS})

mark_as_advanced(DSPSR_INCLUDE_DIR DSPSR_CFLAGS DSPSR_LDFLAGS)
