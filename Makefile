
# Containerised local development environment
PST_MAKE_PATHS=.pst
include $(PST_MAKE_PATHS)/devenv.mk

# The following should be standard includes
# include core makefile targets for release management
include .make/base.mk

# include oci makefile targets for oci management
include .make/oci.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

PROJECT=ska-pst-dsp-tools

DEV_IMAGE_REGISTRY          ?= registry.gitlab.com/ska-telescope/pst/ska-pst-buildtools
DEV_IMAGE                   ?= ska-pst-buildtools
DEV_TAG                     ?= 0.0.11

# Variables populated for local development and oci build.
#       Overriden by CI variables. See .gitlab-cy.yml#L7
PST_OCI_DOCKER_REGISTRY     ?= registry.gitlab.com/ska-telescope/pst/ska-pst-dsp-tools
OCI_REGISTRY ?= $(PST_OCI_DOCKER_REGISTRY)
PST_OCI_DOCKER_TAG          ?= $(VERSION)
ifneq ($(CI_COMMIT_SHORT_SHA),)
PST_OCI_DOCKER_TAG := $(PST_OCI_DOCKER_TAG)-dev.c$(CI_COMMIT_SHORT_SHA)
endif

PSRCHIVE_BASE_IMAGE         ?= ${DEV_IMAGE_REGISTRY}/${DEV_IMAGE}:${DEV_TAG}
DSPSR_BASE_IMAGE            ?= ${OCI_REGISTRY}/ska-pst-psrchive:${PST_OCI_DOCKER_TAG}
DSPSR_BUILDER_IMAGE         ?= ${OCI_REGISTRY}/ska-pst-dspsr-builder:${PST_OCI_DOCKER_TAG}

OCI_IMAGES = ska-pst-psrchive ska-pst-dspsr-builder ska-pst-dspsr
OCI_IMAGES_TO_PUBLISH=ska-pst-psrchive ska-pst-dspsr-builder ska-pst-dspsr
OCI_IMAGE_BUILD_CONTEXT = $(PWD)
OCI_BUILD_ADDITIONAL_ARGS = --build-arg PSRCHIVE_BASE_IMAGE=${PSRCHIVE_BASE_IMAGE} \
	--build-arg DSPSR_BASE_IMAGE=${DSPSR_BASE_IMAGE} \
	--build-arg DSPSR_BUILDER_IMAGE=${DSPSR_BUILDER_IMAGE}


# OS package installation
.PHONY: local-pkg-install
PKG_CLI_CMD ?= apt-get
PKG_CLI_PAYLOAD ?=      # Payload file
PKG_CLI_PARAMETERS ?=   # Package manager installation parameters

local-pkg-install:
	$(PKG_CLI_CMD) $(PKG_CLI_PARAMETERS) `cat $(PKG_CLI_PAYLOAD)`

